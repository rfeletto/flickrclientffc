package it.univr.android.flickrclient.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import it.univr.android.flickrclient.Constants;
import it.univr.android.flickrclient.R;
import it.univr.android.flickrclient.model.Comment;

/*
    Customized adapter used to load comments into the listView
 */

public class CommentAdapter extends ArrayAdapter<Comment> {
    private Context mContext = null;
    private int mLayout;

    public CommentAdapter(Context context, int layoutID, ArrayList<Comment> comments)
    {
        super(context, layoutID, comments);
        mContext = context;
        mLayout = layoutID;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent)
    {
        LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = vi.inflate(mLayout, null);

        Typeface fontAuthor = Typeface.createFromAsset(getContext().getAssets(), Constants.SEMIBOLD_FONT_PATH);
        Typeface fontComment = Typeface.createFromAsset(getContext().getAssets(), Constants.REGULAR_FONT_PATH);
        Typeface fontDate = Typeface.createFromAsset(getContext().getAssets(), Constants.ITALIC_FONT_PATH);



        Comment comment = getItem(position);

        TextView tv_author = (TextView) view.findViewById(R.id.comment_author);
        TextView tv_text = (TextView) view.findViewById(R.id.comment_text);
        TextView tv_date = (TextView) view.findViewById(R.id.comment_date);

        tv_author.setText(comment.getAuthor());
        tv_date.setText(mContext.getResources().getString(R.string.postOn) + " " + comment.getDate());
        tv_text.setText(comment.getText());

        tv_author.setTypeface(fontAuthor);
        tv_text.setTypeface(fontComment);
        tv_date.setTypeface(fontDate);

        return view;
    }
}

package it.univr.android.flickrclient;

public class Constants {

    //Class with constants used in the whole project

    public static final String API_KEY = "388f5641e6dc1ecac49678a156f375df";
    public static final String STANDARD_QUERY = "https://api.flickr.com/services/rest?method=flickr.photos.search&api_key=%s&text=%s&extras=url_s,description,tags&per_page=%s";
    public static final String SAME_AUTHOR_QUERY = "https://api.flickr.com/services/rest?method=flickr.people.getPhotos&api_key=%s&user_id=%s&extras=url_s&per_page=%s";
    public static final String MOST_POPULAR_QUERY = "https://api.flickr.com/services/rest?method=flickr.interestingness.getList&api_key=%s&extras=url_s&per_page=%s";
    public static final String LAST_UPLOADED_QUERY = "https://api.flickr.com/services/rest?method=flickr.photos.getRecent&api_key=%s&extras=url_s&per_page=%s";
    public static final String NUMBER_RESULTS = "50";

    public static final int N_THREAD = 50;

    public static final int STANDARD_TYPE_SEARCH = 1;
    public static final int SAME_AUTHOR_TYPE_SEARCH = 2;
    public static final int MOST_POPULAR_TYPE_SEARCH = 3;
    public static final int LAST_UPLOADED_TYPE_SEARCH = 4;

    public static final String THUMBNAIL_IMAGE = "_s.";
    public static final String DEFAULT_IMAGE = "_m.";
    public static final String HD_IMAGE = "_b.";
    public static final String HD_IMAGE_TO_SHARE = "_x.";

    public static final String COMMENTS_QUERY = "https://api.flickr.com/services/rest?method=flickr.photos.comments.getList&api_key=%s&photo_id=%s&per_page=%s";

    public static final String TITLE = "<font color=#0063db>FlickrClient</font><font color=#ff0084>FFC</font>";

    public static final String TAG_FRAGMENT_SEARCH = "fragment_search";
    public static final String TAG_FRAGMENT_LIST = "fragment_list";
    public static final String TAG_FRAGMENT_HD = "fragment_hd";

    public static final String TOOLBAR_DEFAULT = "toolbar_default";
    public static final String TOOLBAR_HD = "toolbar_hd";
    public static final String TOOLBAR_LIST = "toolbar_list";

    public static final int SHARE_MENU_SELECTION = 1;
    public static final int SAME_AUTHOR_MENU_SELECTION = 2;

    public static final String REGULAR_FONT_PATH = "fonts/Open_Sans/OpenSans-Regular.ttf";
    public static final String BOLD_FONT_PATH = "fonts/Open_Sans/OpenSans-Bold.ttf";
    public static final String SEMIBOLD_FONT_PATH = "fonts/Open_Sans/OpenSans-SemiBold.ttf";
    public static final String ITALIC_FONT_PATH = "fonts/Open_Sans/OpenSans-Italic.ttf";
}

package it.univr.android.flickrclient.view;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import it.univr.android.flickrclient.FlickrClient;
import it.univr.android.flickrclient.MVC;
import it.univr.android.flickrclient.R;

public class TabletView extends LinearLayout implements View {

    private MVC mvc;

    public TabletView(Context context){
        super(context);
    }

    public TabletView(Context context, AttributeSet attributeSet){
        super(context, attributeSet);
    }

    private FragmentManager getFragmentManager(){
        return ((Activity) getContext()).getFragmentManager();
    }

    public AbstractFragment getSearchFragment(){
        return (AbstractFragment) getFragmentManager().findFragmentById(R.id.fragment_search);
    }

    public AbstractFragment getListFragment(){
        return (AbstractFragment) getFragmentManager().findFragmentById(R.id.fragment_list);
    }

    @Override
    protected void onAttachedToWindow(){
        super.onAttachedToWindow();
        mvc = ((FlickrClient)getContext().getApplicationContext()).getMVC();
        mvc.register(this);
    }

    @Override
    protected void onDetachedFromWindow(){
        mvc.unregister(this);
        super.onDetachedFromWindow();
    }

    @Override
    public void onModelChanged() {

        if (getSearchFragment() != null){
            getSearchFragment().onModelChanged();
            getListFragment().onModelChanged();
        }
    }

    @Override
    public void startSpinner() {
        getListFragment().startSpinner();
    }

    @Override
    public void stopSpinner() {
        getListFragment().stopSpinner();
    }
}

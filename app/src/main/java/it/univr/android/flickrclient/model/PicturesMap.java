package it.univr.android.flickrclient.model;

import android.graphics.Bitmap;

import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.ThreadSafe;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import it.univr.android.flickrclient.MVC;
import it.univr.android.flickrclient.view.View;

@ThreadSafe
public class PicturesMap implements Iterable<Map.Entry<String,Bitmap>> {

    private MVC mvc;

    @GuardedBy("itself")
    private final Map<String,Bitmap> map;

    public PicturesMap(MVC mvc) {
        this.mvc = mvc;
        map = Collections.synchronizedMap(new HashMap<String, Bitmap>());
    }


    public void addImage(String url, Bitmap image){
        synchronized (map) {
            if (!map.containsKey(url))
                map.put(url, image);
        }

        mvc.forEachView(View::onModelChanged);
    }

    public Bitmap getImage(String url){
        if(map.containsKey(url))
            return (Bitmap) map.get(url);

        return null;
    }

    public boolean isFetched(String url){
        return map.containsKey(url);
    }


    @Override
    public Iterator<Map.Entry<String, Bitmap>> iterator() {
        return map.entrySet().iterator();
    }
}
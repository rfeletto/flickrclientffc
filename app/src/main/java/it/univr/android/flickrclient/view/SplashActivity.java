package it.univr.android.flickrclient.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread welcomeThread = new Thread() {

            @Override
            public void run() {
                try {
                    super.run();
                    sleep(2500);
                } catch (Exception e) {

                } finally {

                    Intent intent = new Intent(SplashActivity.this, FlickrSearchActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };
        welcomeThread.start();


    }
}

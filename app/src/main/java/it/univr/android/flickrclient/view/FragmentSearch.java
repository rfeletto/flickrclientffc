package it.univr.android.flickrclient.view;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.view.*;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Toast;


import it.univr.android.flickrclient.Constants;
import it.univr.android.flickrclient.FlickrClient;
import it.univr.android.flickrclient.MVC;
import it.univr.android.flickrclient.R;

public class FragmentSearch extends Fragment implements AbstractFragment {
    private SearchView searchView;
    private MVC mvc;

    @Override @UiThread
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        FlickrClient app = (FlickrClient) getActivity().getApplication();
        mvc = app.getMVC();
    }

    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstaceState)
    {
        View view = inflater.inflate(R.layout.fragment_search,container,false);
        View phoneView = (View) getActivity().findViewById(R.id.phoneView);

        //Set the toolbar
        ((FlickrSearchActivity) getActivity()).setToolbar(Constants.TOOLBAR_DEFAULT);


        searchView = (SearchView) view.findViewById(R.id.search_string);
        searchView.setIconifiedByDefault(false);

        //Used to hide the keyboard
        InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(view.getWindowToken(), 0);

        //Defining buttons in the home page
        Button most_popular = (Button) view.findViewById(R.id.most_popular_button);
        Button last_uploaded = (Button) view.findViewById(R.id.last_updated_button);

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), Constants.SEMIBOLD_FONT_PATH);
        most_popular.setTypeface(font);
        last_uploaded.setTypeface(font);

        //Defining behavior when most_popular button is clicked
        most_popular.setOnClickListener(
               __ -> {

                   //Hides keyboard
                    manager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                   //Reset the search view query
                    searchView.setQuery("", false);

                    mvc.model.reset();

                    //Set the toolbar
                    ((FlickrSearchActivity)getActivity()).setToolbar(Constants.TOOLBAR_DEFAULT);

                    //Add new fragment depending on display
                    if(phoneView != null){
                        getActivity().getFragmentManager().beginTransaction().replace(R.id.phoneView, new FragmentList(), Constants.TAG_FRAGMENT_LIST)
                                .addToBackStack(null).commit();
                    } else {
                        FragmentManager fm = (FragmentManager) getActivity().getFragmentManager();
                        Fragment fragmentHD = (Fragment) fm.findFragmentByTag(Constants.TAG_FRAGMENT_HD);
                        Fragment list = (Fragment) fm.findFragmentById(R.id.fragment_list);

                        if(fragmentHD != null){
                            fm.beginTransaction().hide(fragmentHD).show(list).commit();
                        }

                    }

                    mvc.controller.search(getActivity(), null, Constants.MOST_POPULAR_TYPE_SEARCH);


               }
        );

        //Defining behavior when last_uploaded button is clicked
        last_uploaded.setOnClickListener(
                __ -> {

                    //Hides keyboard
                    manager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                    //Reset the search view query
                    searchView.setQuery("", false);

                    mvc.model.reset();

                    //Set the toolbar
                    ((FlickrSearchActivity)getActivity()).setToolbar(Constants.TOOLBAR_DEFAULT);

                    //Add new fragment depending on display
                    if(phoneView != null){
                        getActivity().getFragmentManager().beginTransaction().replace(R.id.phoneView, new FragmentList(), Constants.TAG_FRAGMENT_LIST)
                                .addToBackStack(null).commit();
                    } else {
                        FragmentManager fm = (FragmentManager) getActivity().getFragmentManager();
                        Fragment fragmentHD = (Fragment) fm.findFragmentByTag(Constants.TAG_FRAGMENT_HD);
                        Fragment list = (Fragment) fm.findFragmentById(R.id.fragment_list);

                        if(fragmentHD != null){
                            fm.beginTransaction().hide(fragmentHD).show(list).commit();
                        }

                    }

                    if (((FlickrSearchActivity) getActivity()).checkConnection()){
                        mvc.controller.search(getActivity(), null, Constants.LAST_UPLOADED_TYPE_SEARCH);
                    } else {
                        Toast.makeText(getActivity(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }


                }
        );

        //Defining behavior for the standard research
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {

            @Override
            public boolean onQueryTextSubmit(String query) {
                //Prepare the model for a new research
                mvc.model.reset();

                //Set the toolbar
                ((FlickrSearchActivity) getActivity()).setToolbar(Constants.TOOLBAR_DEFAULT);

                //Add new fragment depending on display
                if(phoneView != null) {
                    getActivity().getFragmentManager()
                            .beginTransaction()
                            .addToBackStack(null)
                            .hide(FragmentSearch.this)
                            .add(R.id.phoneView, new FragmentList(), Constants.TAG_FRAGMENT_LIST)
                            .commit();
                }
                else {
                    FragmentManager fm = (FragmentManager) getActivity().getFragmentManager();
                    Fragment hr = (Fragment) fm.findFragmentByTag(Constants.TAG_FRAGMENT_HD);
                    Fragment list = (Fragment) fm.findFragmentById(R.id.fragment_list);

                    if (hr != null){
                        fm.beginTransaction().hide(hr).show(list).commit();
                    }
                }

                //Checking internet connection
                if (((FlickrSearchActivity) getActivity()).checkConnection()){
                    mvc.controller.search(getActivity(),searchView.getQuery().toString(),Constants.STANDARD_TYPE_SEARCH);
                } else {
                    Toast.makeText(getActivity(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });



        return view;
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        onModelChanged();
    }

    @Override @UiThread
    public void onStart()
    {
        super.onStart();
        mvc.register(this);
        onModelChanged();
    }

    @Override @UiThread
    public void onStop()
    {
        mvc.unregister(this);
        super.onStop();
    }

    @Override
    public void onModelChanged() {

    }

    @Override
    public void startSpinner() {

    }

    @Override
    public void stopSpinner() {

    }
}

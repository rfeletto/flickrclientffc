package it.univr.android.flickrclient.model;

import android.graphics.Bitmap;

import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.Immutable;
import net.jcip.annotations.ThreadSafe;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import it.univr.android.flickrclient.MVC;
import it.univr.android.flickrclient.view.View;

@ThreadSafe
public class Model {
    private MVC mvc;
    private PicturesMap hdMap;
    private PicturesMap thumbnailMap;
    private CommentList commentList;

    @GuardedBy("itself")
    private final LinkedList<PictureInfo> pictureInfos = new LinkedList<>();

    @Immutable
    public static class PictureInfo {
        private final String title;
        private final String url;
        private final String flickrID;
        private final String author;

        public PictureInfo(String title, String url, String flickID,String author) {
            this.title = title;
            this.url = url;
            this.flickrID = flickID;
            this.author = author;

        }

        public String getTitle()
        {
            return title;
        }

        public String getUrl()
        {
            return url;
        }

        public String getFlickrID() {return flickrID;}

        public String getAuthor() {return author;}

    }

    //MVC Initialization

    public void setMVC(MVC mvc) {
        this.mvc = mvc;
        hdMap = new PicturesMap(mvc);
        thumbnailMap = new PicturesMap(mvc);
        commentList = new CommentList(mvc);
    }

    //PictureInfo External Function

    public void storePictureInfos(Iterable<PictureInfo> pictureInfos) {
        synchronized (this.pictureInfos) {
            this.pictureInfos.clear();
            for (PictureInfo pi: pictureInfos)
                this.pictureInfos.add(pi);
        }

        mvc.forEachView(View::onModelChanged);
    }

    public Iterator<PictureInfo> getPictureInfos() {
        synchronized (pictureInfos) {
            ArrayList<PictureInfo> new_list = new ArrayList<>();
            for (PictureInfo pi: pictureInfos)
                new_list.add(pi);

            return new_list.iterator();
        }
    }

    public void reset()
    {
        synchronized (pictureInfos) {
            this.pictureInfos.clear();
        }
    }

    public ArrayList<PictureInfo> getPictureInfosAdapter()
    {
        synchronized (pictureInfos)
        {
            ArrayList<PictureInfo> list = new ArrayList<>();
            for(PictureInfo pi: pictureInfos)
                list.add(pi);
            return list;
        }
    }

    //Thumbnail External Function

    public boolean isThumbnailFetched(String url) {
        return thumbnailMap.isFetched(url);
    }

    public void addThumbnailImage(String url, Bitmap image) {
        thumbnailMap.addImage(url, image);
    }

    public Bitmap getThumbnailImage(String url) {
        return thumbnailMap.getImage(url);
    }

    //HD External Function

    public Bitmap getHDImage(String url) {
        return hdMap.getImage(url);
    }

    public void addHDImage(String url, Bitmap image) {
        hdMap.addImage(url, image);
    }

    //Comment External Function

    public ArrayList<Comment> getCommentListForAdapter() {
        return commentList.getList();
    }

    public Iterator<Comment> getCommentList() {
        synchronized (commentList) {
            return commentList.iterator();
        }
    }

    public void storeCommentList(ArrayList<Comment> list) {
        commentList.storeList(list);
    }


}
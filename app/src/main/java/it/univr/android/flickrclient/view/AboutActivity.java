package it.univr.android.flickrclient.view;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import it.univr.android.flickrclient.Constants;
import it.univr.android.flickrclient.R;

/*
    Class in which is specified the info section, with icon, information about authors and about the app
 */

public class AboutActivity extends AppCompatActivity {

    @Override @UiThread
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.about_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Typeface fontRegular = Typeface.createFromAsset(getAssets(), Constants.REGULAR_FONT_PATH);
        Typeface fontSemiBold = Typeface.createFromAsset(getAssets(), Constants.SEMIBOLD_FONT_PATH);
        Typeface fontBold = Typeface.createFromAsset(getAssets(), Constants.BOLD_FONT_PATH);



        if (toolbar != null) {
            setSupportActionBar(toolbar);
            String title = Constants.TITLE;
            if (getSupportActionBar() != null){
                TextView mTitle = (TextView) findViewById(R.id.toolbar_title);
                mTitle.setTypeface(fontRegular);
                mTitle.setText(Html.fromHtml(title));
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        TextView appName = (TextView)findViewById(R.id.app_name);
        appName.setTypeface(fontSemiBold);
        appName.setText(R.string.app_name);

        TextView auth = (TextView)findViewById(R.id.app_auth);
        auth.setTypeface(fontBold);
        auth.setText(R.string.authors);

        TextView authors = (TextView)findViewById(R.id.app_authors);
        authors.setTypeface(fontRegular);
        authors.setText(R.string.app_authors);

        TextView version = (TextView)findViewById(R.id.app_version);
        version.setTypeface(fontRegular);
        version.setText(R.string.app_version);

        TextView date = (TextView)findViewById(R.id.app_date);
        date.setTypeface(fontRegular);
        date.setText(R.string.app_date);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.toolbar_hd, menu);

        MenuItem share = menu.findItem(R.id.share);
        share.setVisible(false);

        MenuItem about = menu.findItem(R.id.about);
        about.setVisible(false);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}

package it.univr.android.flickrclient.view;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import it.univr.android.flickrclient.Constants;
import it.univr.android.flickrclient.FlickrClient;
import it.univr.android.flickrclient.MVC;
import it.univr.android.flickrclient.R;

public class FlickrSearchActivity extends AppCompatActivity implements View {

    private MVC mvc;
    private boolean doublePanel = false;
    Toolbar toolbar;
    boolean toolbar_hd = false;
    String sharing_url = null;

    @Override @UiThread
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);


        FlickrClient app = (FlickrClient) getApplication();
        mvc = app.getMVC();


        setContentView(R.layout.activity_search);

        //Hides Keyboard on Startup
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        //Verify if phoneView or a tabletView
        View view = (View) findViewById(R.id.tabletView);
        if (view != null) {
            doublePanel = true;
        }

        //Set the toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        Typeface font = Typeface.createFromAsset(getAssets(), Constants.REGULAR_FONT_PATH);


        if (toolbar != null){
            setSupportActionBar(toolbar);
            String title = Constants.TITLE;
            if (getSupportActionBar() != null){
                TextView mTitle = (TextView) findViewById(R.id.toolbar_title);
                mTitle.setTypeface(font);
                mTitle.setText(Html.fromHtml(title));

            }
        }

        FragmentManager fm = (FragmentManager) getFragmentManager();
        Fragment search = (Fragment) fm.findFragmentByTag(Constants.TAG_FRAGMENT_SEARCH);

        if(!doublePanel && search == null) {
            Fragment newFragment = new FragmentSearch();
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.add(R.id.phoneView, newFragment, Constants.TAG_FRAGMENT_SEARCH);
            ft.commit();
        }
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    //Defining the different types of toolbar, it has to show the share button only in the HD Toolbar
    //and in the home page the back button has not to be showed
    public void setToolbar(String str){

        switch (str){
            case Constants.TOOLBAR_HD:
                toolbar_hd = true;
                break;
            default:
                toolbar_hd = false;
        }

        if((str.equals(Constants.TOOLBAR_HD) || str.equals(Constants.TOOLBAR_LIST)) && (!doublePanel)){
            if (getSupportActionBar() != null){
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        } else if (doublePanel && str.equals(Constants.TOOLBAR_HD)){
            if (getSupportActionBar() != null){
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        } else {
            if (getSupportActionBar() != null){
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            }
        }

        supportInvalidateOptionsMenu();

    }

    //Function that checks if the connection is available
    public boolean checkConnection(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.toolbar_hd, menu);

        MenuItem share = menu.findItem(R.id.share);
        share.setVisible(toolbar_hd);

        return super.onCreateOptionsMenu(menu);
    }

    public void setUrlToShare(String url) {

        sharing_url = url;
    }

    public void resetUrlToShare() {

        sharing_url = null;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share:
                if (checkConnection())
                    if (sharing_url != null)
                        mvc.controller.fetchImage(Constants.HD_IMAGE_TO_SHARE, sharing_url, this);
                    else
                        Toast.makeText(this, R.string.share_url_error, Toast.LENGTH_SHORT).show();
                else
                   Toast.makeText(this, R.string.connection_error, Toast.LENGTH_SHORT).show();

                break;

            case R.id.about:
                Intent intent = new Intent(FlickrSearchActivity.this, AboutActivity.class);
                startActivity(intent);
                break;

            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();

        FragmentManager fm = (FragmentManager) getFragmentManager();
        Fragment list = (Fragment) fm.findFragmentByTag(Constants.TAG_FRAGMENT_LIST);

        if (list != null){
            setToolbar(Constants.TOOLBAR_LIST);
        }
        else {
            setToolbar(Constants.TOOLBAR_DEFAULT);
        }
    }

    @Override @UiThread
    protected void onStart()
    {
        super.onStart();
        mvc.register(this);
        onModelChanged();
    }

    @Override @UiThread
    protected void onStop()
    {
        mvc.unregister(this);
        super.onStop();
    }

    @Override
    public void onModelChanged() {

    }

    @Override
    public void startSpinner() {

    }

    @Override
    public void stopSpinner() {

    }


}


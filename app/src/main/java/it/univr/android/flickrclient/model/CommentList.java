package it.univr.android.flickrclient.model;

import net.jcip.annotations.GuardedBy;

import java.util.ArrayList;
import java.util.Iterator;

import it.univr.android.flickrclient.MVC;
import it.univr.android.flickrclient.view.View;

public class CommentList implements Iterable<Comment> {
    private MVC mvc;
    private String url;
    @GuardedBy("itself") private final ArrayList<Comment> list;

    public CommentList() {
        list = new ArrayList<Comment>();
        url = null;
    }

    public CommentList(MVC mvc){
        this.mvc = mvc;
        list = new ArrayList<>();
        url = null;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public String getUrl(){
        return url;
    }

    public ArrayList<Comment> getList() {
        synchronized (list) {
            ArrayList<Comment> new_list = new ArrayList<Comment>();

            for(Comment c: list)
                new_list.add(c);

            return new_list;
        }
    }

    public void storeList(Iterable<Comment> comments) {
        synchronized (list) {
            this.resetList();

            for (Comment c: comments)
                list.add(c);
        }

        mvc.forEachView(View::onModelChanged);
    }

    public void resetList() {
        synchronized (list) {
            list.clear();
        }
    }

    @Override
    public Iterator<Comment> iterator() {
        return list.iterator();
    }
}
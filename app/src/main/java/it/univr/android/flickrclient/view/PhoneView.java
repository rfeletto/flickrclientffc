package it.univr.android.flickrclient.view;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import it.univr.android.flickrclient.Constants;
import it.univr.android.flickrclient.FlickrClient;
import it.univr.android.flickrclient.MVC;
import it.univr.android.flickrclient.R;

public class PhoneView extends FrameLayout implements View {

    private MVC mvc;

    public PhoneView(Context context)
    {
        super(context);
    }

    public PhoneView(Context context, AttributeSet attrs)
    {
        super(context,attrs);
    }

    private FragmentManager getFragmentManager()
    {
        return ((Activity) getContext()).getFragmentManager();
    }

    public AbstractFragment getFragment() {

        if(getFragmentManager().findFragmentByTag(Constants.TAG_FRAGMENT_SEARCH) != null) {
            return (AbstractFragment) getFragmentManager().findFragmentByTag(Constants.TAG_FRAGMENT_SEARCH);
        }
        else if (getFragmentManager().findFragmentById(R.id.fragment_list) != null) {
            return (AbstractFragment) getFragmentManager().findFragmentById(R.id.fragment_list);
        }
        else {
            return null;
        }
    }


    @Override
    public void onModelChanged() {
        getFragment().onModelChanged();
    }

    @Override
    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        mvc = ((FlickrClient) getContext().getApplicationContext()).getMVC();
        mvc.register(this);
    }

    @Override
    protected void onDetachedFromWindow()
    {
        mvc.unregister(this);
        super.onDetachedFromWindow();
    }

    @Override
    public void startSpinner()
    {
        getFragment().startSpinner();
    }

    @Override
    public void stopSpinner() {
        getFragment().stopSpinner();
    }
}

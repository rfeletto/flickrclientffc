package it.univr.android.flickrclient.view;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import it.univr.android.flickrclient.FlickrClient;
import it.univr.android.flickrclient.MVC;
import it.univr.android.flickrclient.R;

import static android.os.Build.VERSION_CODES.M;

/*
    Class that defines the behavior when a picture is going to be shared
    It checks also for the permissions
 */
public class ShareActivity extends AppCompatActivity {

    private MVC mvc;
    private String url;
    private Uri uri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FlickrClient app = (FlickrClient) getApplication();
        mvc = app.getMVC();

        // Gets Intent and URL from extras
        Intent intent = getIntent();
        url = intent.getStringExtra("url");

        // Checks if permission has been granted, or ask for it
        getPermissionToWriteExt();
    }

    //Function that check if the sharing is allowed
    public void getPermissionToWriteExt() {

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= M) {
                if (shouldShowRequestPermissionRationale(
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                }

                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);

            }
        } else {
            // Starts sharing if permission has been granted
            share();
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        if (requestCode == 1) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, getString(R.string.write_permission_granted), Toast.LENGTH_SHORT).show();
                share();
            } else {
                Toast.makeText(this, getString(R.string.write_permission_denied), Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    public void share() {
        if (url != null) {
            Bitmap image = mvc.model.getHDImage(url);

            // Gets image path
            String path = MediaStore.Images.Media.insertImage(getContentResolver(), image, "Title", null);

            // Parses path to URI
            uri = Uri.parse(path);
        } else {
            Toast.makeText(this, R.string.URI_error, Toast.LENGTH_SHORT).show();
        }

        if (uri != null) {
            // Starts sharing intent
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("image/jpeg");
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(Intent.createChooser(intent, getString(R.string.share)));
        } else {
            Toast.makeText(this, R.string.share_error, Toast.LENGTH_SHORT).show();
        }

        finish();
    }
}



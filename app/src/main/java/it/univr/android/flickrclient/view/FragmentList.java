package it.univr.android.flickrclient.view;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.view.*;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Iterator;

import it.univr.android.flickrclient.Constants;
import it.univr.android.flickrclient.FlickrClient;
import it.univr.android.flickrclient.MVC;
import it.univr.android.flickrclient.R;
import it.univr.android.flickrclient.adapter.ListAdapter;
import it.univr.android.flickrclient.model.Model;

public class FragmentList extends Fragment implements AbstractFragment {
    private MVC mvc;
    private ProgressBar pb;
    private ListAdapter customAdapter;
    private ListView lView;
    private String urlToShare;
    private String authorSelected;

    @Override @UiThread
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        FlickrClient app = (FlickrClient) getActivity().getApplication();
        mvc = app.getMVC();
    }

    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_list,container,false);
        pb = (ProgressBar) view.findViewById(R.id.progressBar);

        //Check if internet connection is available
        if (!((FlickrSearchActivity) getActivity()).checkConnection()){
            Toast.makeText(getActivity(), R.string.connection_error, Toast.LENGTH_SHORT).show();
            pb.setVisibility(ProgressBar.INVISIBLE);
        }

        //Setting the custom adapter (ListAdapter)
        customAdapter = new ListAdapter(getActivity(),R.layout.picture_row, mvc.model.getPictureInfosAdapter());
        lView = (ListView) view.findViewById(android.R.id.list);
        lView.setEmptyView(view.findViewById(R.id.noImage));
        lView.setAdapter(customAdapter);
        registerForContextMenu(lView);

        View phoneView = (View) getActivity().findViewById(R.id.phoneView);


        //Defining behavior when an image is clicked
        lView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, android.view.View view, int position, long id) {

                TextView tv_selected_url = (TextView) view.findViewById(R.id.item_url);
                String selected_url = tv_selected_url.getText().toString();

                TextView tv_selected_id = (TextView) view.findViewById(R.id.item_id);
                String selected_id = tv_selected_id.getText().toString();

                // Puts arguments to pass in HDFragment
                Bundle args = new Bundle();
                args.putString("url", selected_url);
                args.putString("id",selected_id);

                // Defines new Intent to be started, with argument
                HDFragment newFragment = new HDFragment();
                newFragment.setArguments(args);

                // Add new HD fragment, depending on display
                if (phoneView != null) {
                    getActivity().getFragmentManager()
                            .beginTransaction()
                            .addToBackStack(null)
                            .hide(FragmentList.this)
                            .add(R.id.phoneView, newFragment, Constants.TAG_FRAGMENT_HD)
                            .commit();
                }
                else {
                    getActivity().getFragmentManager()
                            .beginTransaction()
                            .addToBackStack(null)
                            .hide(FragmentList.this)
                            .add(R.id.fragment_list_parent, newFragment, Constants.TAG_FRAGMENT_HD)
                            .commit();
                }

            }
        });


        return view;

    }

    //Creates menu for sharing the selected image or for a same author research
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(R.string.menu_context_title);
        menu.add(0, Constants.SHARE_MENU_SELECTION, 0, R.string.menu_item_share);
        menu.add(0, Constants.SAME_AUTHOR_MENU_SELECTION, 0, R.string.menu_item_same_author);

        ListView lv = (ListView) v;
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        Model.PictureInfo pictureInfo = (Model.PictureInfo) lv.getItemAtPosition(info.position);

        urlToShare = pictureInfo.getUrl();
        authorSelected = pictureInfo.getAuthor();
    }

    //Defines the behavior when an item in the menu is clicked
    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case Constants.SHARE_MENU_SELECTION:
                if(isAdded()){
                    if (((FlickrSearchActivity) getActivity()).checkConnection()) {
                        if (urlToShare != null) {
                            mvc.controller.fetchImage(Constants.HD_IMAGE_TO_SHARE, urlToShare, getActivity());
                        } else {
                            Toast.makeText(getActivity(), R.string.share_url_error, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case Constants.SAME_AUTHOR_MENU_SELECTION:
                customAdapter.clear();

                if (((FlickrSearchActivity) getActivity()).checkConnection()){
                    mvc.controller.search(getActivity(),authorSelected,Constants.SAME_AUTHOR_TYPE_SEARCH);
                } else {
                    Toast.makeText(getActivity(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                }
                break;
        }

        return true;
    }

    @Override
    public void onResume()
    {
        ((FlickrSearchActivity) getActivity()).setToolbar(Constants.TOOLBAR_LIST);

        super.onResume();
    }

    @Override
    public void onModelChanged() {

        if(lView.getAdapter() == null)
        {
            lView.setAdapter(customAdapter);
        }
        else {
            customAdapter.setNotifyOnChange(false);
            customAdapter.clear();
            for (Iterator<Model.PictureInfo> it = mvc.model.getPictureInfos(); it.hasNext(); )
                customAdapter.add(it.next());
            customAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void startSpinner() {
        pb.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    public void stopSpinner() {
        pb.setVisibility(View.GONE);
    }

    @Override @UiThread
    public void onActivityCreated(@Nullable Bundle savedInstance)
    {
        super.onActivityCreated(savedInstance);
        onModelChanged();
    }

    @Override @UiThread
    public void onStart() {
        super.onStart();
        mvc.register(this);
        onModelChanged();
    }

    @Override @UiThread
    public void onStop() {
        mvc.unregister(this);
        super.onStop();
    }
}

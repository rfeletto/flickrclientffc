package it.univr.android.flickrclient.model;

import java.util.Calendar;

public class Comment {
    private String author;
    private String text;
    private Long timestamp;

    public Comment(String author, String text, Long timestamp)
    {
        if(author != null) this.author = author;
        else this.author = "";

        if(text != null) this.text = text;
        else this.text = "";

        this.timestamp = timestamp;
    }

    public String getAuthor() {return this.author;}

    public String getText() {return this.text;}

    public String getDate()
    {
        if (this.timestamp == 0) return "";

        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(this.timestamp * 1000);

        int day = date.get(Calendar.DAY_OF_MONTH);
        int month = date.get(Calendar.MONTH) + 1;
        int year = date.get(Calendar.YEAR);

        return day+"/"+month+"/"+year;
    }
}

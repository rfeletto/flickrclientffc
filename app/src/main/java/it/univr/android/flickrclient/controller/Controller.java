package it.univr.android.flickrclient.controller;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.UiThread;
import android.support.annotation.WorkerThread;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import it.univr.android.flickrclient.MVC;
import it.univr.android.flickrclient.R;
import it.univr.android.flickrclient.model.Comment;
import it.univr.android.flickrclient.model.Model;
import it.univr.android.flickrclient.view.ShareActivity;
import it.univr.android.flickrclient.view.View;

import static android.R.attr.type;
import it.univr.android.flickrclient.Constants;

public class Controller {
    private MVC mvc;
    private static final Executor exPictureInfo = Executors.newSingleThreadExecutor();
    private static final Executor exThumbnail = Executors.newFixedThreadPool(Constants.N_THREAD);
    private static final Executor executorComments = Executors.newSingleThreadExecutor();


    public void setMVC(MVC mvc) {
        this.mvc = mvc;
    }

    public void search(Activity activity, String searchString, int queryType) {
        PicturesInfoFetcher picInfoFetcher =  new PicturesInfoFetcher(activity,searchString,queryType);
        exPictureInfo.execute(picInfoFetcher);
        mvc.forEachView(View::startSpinner);
    }

    //Recovering infos from Flickr
    private class PicturesInfoFetcher implements Runnable {

        private String searchString;
        private final Activity mActivity;
        private int queryType;

        public PicturesInfoFetcher(Activity activity, String searchString, int queryType)
        {
            this.mActivity = activity;
            this.searchString = searchString;
            this.queryType = queryType;
        }

        @Override
        public void run() {
            ArrayList<Model.PictureInfo> pictureInfos = fetchPicture(searchString,queryType);

            mvc.forEachView(View::stopSpinner);

            for (Model.PictureInfo pi : pictureInfos) {
                if (!mvc.model.isThumbnailFetched(pi.getUrl()))
                    fetchImage(Constants.THUMBNAIL_IMAGE, pi.getUrl(), mActivity);
            }

            mvc.model.storePictureInfos(pictureInfos);
        }

        //Worker thread in which is specified the research type
        @WorkerThread
        private ArrayList<Model.PictureInfo> fetchPicture(String searchString, int queryType)
        {
            String query = "";
            switch (queryType)
            {
                case Constants.STANDARD_TYPE_SEARCH:     //Search by keyword
                    query = String.format(Constants.STANDARD_QUERY,
                            Constants.API_KEY,
                            searchString,
                            Constants.NUMBER_RESULTS);
                    break;
                case Constants.SAME_AUTHOR_TYPE_SEARCH:     //Same author
                    query = String.format(Constants.SAME_AUTHOR_QUERY,
                            Constants.API_KEY,
                            searchString,
                            Constants.NUMBER_RESULTS);
                    break;
                case Constants.MOST_POPULAR_TYPE_SEARCH:     //Most popular
                    query = String.format(Constants.MOST_POPULAR_QUERY,
                            Constants.API_KEY,
                            Constants.NUMBER_RESULTS);
                    break;
                case Constants.LAST_UPLOADED_TYPE_SEARCH:     //Last updated
                    query = String.format(Constants.LAST_UPLOADED_QUERY,
                            Constants.API_KEY,
                            Constants.NUMBER_RESULTS);
                    break;
            }

            try
            {
                URL url = new URL(query);
                URLConnection conn = url.openConnection();

                String ans = "";
                BufferedReader in = null;

                try
                {
                    in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                    String row;
                    while((row = in.readLine()) != null)
                    {
                        ans += row + "\n";
                    }
                }
                finally
                {
                    if(in != null)
                        in.close();
                }

                return parse(ans);
            }
            catch (IOException e)
            {
                return new ArrayList<>();
            }
        }

        //Parse Flickr XML and gets the specific fields
        private ArrayList<Model.PictureInfo> parse(String answer)
        {
            ArrayList<Model.PictureInfo> infos = new ArrayList<>();

            int nextPhoto = -1;
            do {
                nextPhoto = answer.indexOf("<photo id", nextPhoto + 1);
                if (nextPhoto >= 0) {
                    String flickrID = answer.substring(nextPhoto + 11, answer.indexOf("\"",nextPhoto + 11));
                    int titlePos = answer.indexOf("title=", nextPhoto) + 7;
                    int url_sPos = answer.indexOf("url_s=", nextPhoto) + 7;
                    int authorPos = answer.indexOf("owner=", nextPhoto) + 7;
                    String title = answer.substring(titlePos, answer.indexOf("\"", titlePos + 1));
                    String url_z = answer.substring(url_sPos, answer.indexOf("\"", url_sPos + 1));
                    String author = answer.substring(authorPos, answer.indexOf("\"", authorPos));
                    infos.add(new Model.PictureInfo(title, url_z,flickrID,author));


                }
            }
            while (nextPhoto != -1);

            return infos;
        }
    }

    @UiThread
    public void fetchImage(String type, String urlString, Activity activity) {
        ImageDownloader imageDownloader = new ImageDownloader(type, urlString, activity);
        exThumbnail.execute(imageDownloader);
    }

    // Runnable to download image thumbnail
    private class ImageDownloader implements Runnable {
        private final static String TAG = "ImageDownloader";
        private String mUrlString;
        private String mtype;
        private final Activity mActivity;

        public ImageDownloader(String type, String urlString, Activity activity){
            mUrlString = urlString;
            mActivity = activity;
            mtype = type;
        }

        //Worker thread in which is specified the image type to be downloaded
        @WorkerThread
        public void run() {

            // doInBackground
            Bitmap image;

            String newUrl;
            switch (mtype) {
                case Constants.THUMBNAIL_IMAGE:
                    newUrl = mUrlString.replace(Constants.DEFAULT_IMAGE, Constants.THUMBNAIL_IMAGE);
                    break;
                case Constants.HD_IMAGE:
                    newUrl = mUrlString.replace(Constants.DEFAULT_IMAGE, Constants.HD_IMAGE);
                    break;
                default:
                    newUrl = mUrlString;
            }

            try {
                URL url = new URL(newUrl);
                image = BitmapFactory.decodeStream((InputStream) url.getContent());

            } catch(IOException e) {
                image = null;
            }

            // onPostExecute
            switch (mtype){
                case Constants.HD_IMAGE:
                    if (image != null) mvc.model.addHDImage(mUrlString, image);
                    break;
                case Constants.HD_IMAGE_TO_SHARE:
                    if (image != null) {
                        mvc.model.addHDImage(mUrlString, image);

                        Intent intent = new Intent(mActivity, ShareActivity.class);
                        intent.putExtra("url", mUrlString);

                        mActivity.startActivity(intent);
                    } else {
                      Toast.makeText(mActivity, R.string.download_error, Toast.LENGTH_SHORT).show();
                    }
                    break;
                default:
                    if (image != null) mvc.model.addThumbnailImage(mUrlString, image);
            }
        }
    }

    @UiThread
    public void fetchComment(String flickrID, String url)
    {
        CommentDownloader commentDownloader = new CommentDownloader(flickrID,url);
        executorComments.execute(commentDownloader);
    }

    //Recovering comments from Flickr
    private class CommentDownloader implements Runnable
    {
        private String flickrID;
        private String url;

        public CommentDownloader(String flickrID, String url)
        {
            this.flickrID = flickrID;
            this.url = url;
        }

        @Override
        public void run()
        {
            mvc.forEachView(View::startSpinner);

            ArrayList<Comment> comments = fetchCommentInfo(flickrID);
            mvc.model.storeCommentList(comments);
            mvc.forEachView(View::stopSpinner);
        }

        //Worker thread in which is set up the comment list
        @WorkerThread
        private ArrayList<Comment> fetchCommentInfo(String flickrID)
        {
            String query = String.format(Constants.COMMENTS_QUERY,
                    Constants.API_KEY,
                    flickrID,
                    Constants.NUMBER_RESULTS);

            try {
                URL url = new URL(query);
                URLConnection conn = url.openConnection();
                String answer = "";

                BufferedReader in = null;
                try {
                    in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                    String line;
                    while ((line = in.readLine()) != null) {
                        answer += line + "\n";
                    }
                }
                finally {
                    if (in != null)
                        in.close();
                }

                return parse(answer);
            }
            catch (IOException e) {
                return new ArrayList<>();
            }
        }

        //Parse Flickr XML and gets the specific fields
        private ArrayList<Comment> parse(String answer) {
            ArrayList<Comment> comments = new ArrayList<>();

            int nextComment = -1;
            do {
                nextComment = answer.indexOf("<comment id", nextComment + 1);
                if (nextComment >= 0) {
                    int authorPos = answer.indexOf("authorname=", nextComment) + 12;
                    int datePos = answer.indexOf("datecreate=", nextComment) + 12;
                    int textPos = answer.indexOf("\">", nextComment) + 2;
                    String author = answer.substring(authorPos, answer.indexOf("\"", authorPos));
                    String date = answer.substring(datePos, answer.indexOf("\"", datePos));
                    String text = answer.substring(textPos, answer.indexOf("</comment>", textPos));

                    Long dateLong = Long.parseLong(date);

                    comments.add(new Comment(author, text, dateLong));
                }
            }
            while (nextComment != -1);

            return comments;
        }
    }
}
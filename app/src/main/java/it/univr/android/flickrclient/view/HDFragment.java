package it.univr.android.flickrclient.view;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Iterator;

import it.univr.android.flickrclient.Constants;
import it.univr.android.flickrclient.FlickrClient;
import it.univr.android.flickrclient.MVC;
import it.univr.android.flickrclient.R;
import it.univr.android.flickrclient.adapter.CommentAdapter;
import it.univr.android.flickrclient.model.Comment;

public class HDFragment extends Fragment implements AbstractFragment {

    private MVC mvc;
    private String url = null;
    private ImageView imageView;
    private ProgressBar mProgressBar;
    private CommentAdapter commentAdapter;
    private ListView commentList;
    private String id = null;
    private TextView noCommnet;

    @Override @UiThread
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FlickrClient app = (FlickrClient) getActivity().getApplication();
        mvc = app.getMVC();
    }

    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        android.view.View view = inflater.inflate(R.layout.fragment_hd, container, false);
        noCommnet = (TextView) view.findViewById(R.id.noComment);

        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        //Check if internet connection is available
        if (!((FlickrSearchActivity) getActivity()).checkConnection()){
            Toast.makeText(getActivity(), R.string.connection_error, Toast.LENGTH_SHORT).show();
            mProgressBar.setVisibility(ProgressBar.INVISIBLE);
        }

        // Gets selected picture arguments
        if (getArguments() != null) {
            url = getArguments().getString("url");
            id = getArguments().getString("id");
        }

        //Set the toolbar
        ((FlickrSearchActivity) getActivity()).setToolbar(Constants.TOOLBAR_HD);

        mvc.controller.fetchImage(Constants.HD_IMAGE, url, getActivity());
        mvc.controller.fetchComment(id,url);

        //Setting the custom adapter (CommentAdapter)
        commentAdapter = new CommentAdapter(getActivity(), R.layout.comment_row, mvc.model.getCommentListForAdapter());
        commentList = (ListView) view.findViewById(android.R.id.list);
        commentList.setAdapter(commentAdapter);

        View header = (View) inflater.inflate(R.layout.header_hd, commentList, false);
        commentList.addHeaderView(header);
        imageView = (ImageView) view.findViewById(R.id.image_hd);

        //Set HD ImageView
        imageView.setImageBitmap(mvc.model.getHDImage(url));

        return view;

    }


    @Override @UiThread
    public void onStart() {
        super.onStart();
        mvc.register(this);
        onModelChanged();
    }

    @Override @UiThread
    public void onStop() {
        ((FlickrSearchActivity) getActivity()).resetUrlToShare();

        mvc.unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {

        ((FlickrSearchActivity) getActivity()).setToolbar(Constants.TOOLBAR_HD);
        ((FlickrSearchActivity) getActivity()).setUrlToShare(url);

        super.onResume();
    }

    @Override
    public void onModelChanged() {

        //Check if imageView is set
        if(hasNullOrEmptyDrawable(imageView))
            imageView.setImageBitmap(mvc.model.getHDImage(url));

        // Sets adapter if not already done
        if (commentList.getAdapter() == null) {
            commentList.setAdapter(commentAdapter);
        } else {
            // Adds comments to ListView or display a message if picture has no comments
            commentAdapter.setNotifyOnChange(false);
            commentAdapter.clear();
            noCommnet.setVisibility(View.GONE);

            Iterator<Comment> it = mvc.model.getCommentList();

            if (it.hasNext())
                for (; it.hasNext(); ) {
                    Comment actual = it.next();
                    commentAdapter.add(actual);
                }
            else {
                noCommnet.setVisibility(View.VISIBLE);
            }

            commentAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void startSpinner() {

        mProgressBar.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    public void stopSpinner() {

        mProgressBar.setVisibility(ProgressBar.GONE);
    }

    //Method for checking null or empty imageview
    public static boolean hasNullOrEmptyDrawable(ImageView iv)
    {
        Drawable drawable = iv.getDrawable();
        BitmapDrawable bitmapDrawable = drawable instanceof BitmapDrawable ? (BitmapDrawable)drawable : null;

        return bitmapDrawable == null || bitmapDrawable.getBitmap() == null;
    }
}

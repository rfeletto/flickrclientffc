package it.univr.android.flickrclient.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import it.univr.android.flickrclient.Constants;
import it.univr.android.flickrclient.FlickrClient;
import it.univr.android.flickrclient.MVC;
import it.univr.android.flickrclient.R;
import it.univr.android.flickrclient.model.Model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import it.univr.android.flickrclient.FlickrClient;
import it.univr.android.flickrclient.MVC;
import it.univr.android.flickrclient.R;
import it.univr.android.flickrclient.model.Model;

/*
    Customized adapter that load items in the pictures list
 */

public class ListAdapter extends ArrayAdapter<Model.PictureInfo> {
    private Context mContext;
    private int mLayout;
    private MVC mvc;

    public ListAdapter(Context context, int layoutID, ArrayList<Model.PictureInfo> pic)
    {
        super(context,layoutID,pic);
        mContext = context;
        mLayout = layoutID;

        FlickrClient app = (FlickrClient) mContext.getApplicationContext();
        mvc = app.getMVC();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(mLayout, null);


        Model.PictureInfo picInfo = getItem(position);

        String url = picInfo.getUrl();


        TextView tv_title = (TextView) view.findViewById(R.id.image_title);
        TextView tv_url = (TextView) view.findViewById(R.id.item_url);
        TextView tv_id = (TextView) view.findViewById(R.id.item_id);
        ImageView iv_icon = (ImageView) view.findViewById(R.id.iconized_image);

        tv_title.setText(picInfo.getTitle());
        tv_url.setText(url);
        tv_id.setText(picInfo.getFlickrID());

        Typeface font = Typeface.createFromAsset(getContext().getAssets(), Constants.REGULAR_FONT_PATH);
        tv_title.setTypeface(font);


        if(mvc.model.isThumbnailFetched(url)) {
            Bitmap image = mvc.model.getThumbnailImage(url);

            if (image != null) iv_icon.setImageBitmap(mvc.model.getThumbnailImage(url));
        }

        return view;
    }

}
package it.univr.android.flickrclient.view;

import android.support.annotation.UiThread;

public interface AbstractFragment extends View {
    @UiThread
    void onModelChanged();
    void startSpinner();
    void stopSpinner();
}
